package codemdg.tagextractor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Solution {
    protected static List<String> results = new ArrayList<>();
    protected static String rgxFindOutTags = "<[^/]*?>";
    protected static Pattern patternFindOutTags = Pattern.compile(rgxFindOutTags);

    protected static List<String> tagsExtractor(String line) {
        Matcher mtFindOutTags = patternFindOutTags.matcher(line);
        Stream<MatchResult> matches = mtFindOutTags.results();
        List<String> tagsOfLine = new ArrayList<>();

        /*
         * Go through tags and remove "<,>" because we will build another string regex based on the tags
         * found by mtFindOutTags
         */
        matches.forEach(e -> {
            String s = e.group(0).replaceAll("[<,>]", "");
            tagsOfLine.add(s.equals("") ? "None" : s);
        });

        return tagsOfLine;
    }

    protected static List<String> extractor(String line, List<String> tagsOfLine) {
        String rgxCheckTagContent = "(.*)";
        String rgxCheckContent;
        Pattern patternCheckContent;
        Pattern patternFindOutTags = Pattern.compile(rgxFindOutTags);

        String s, hashedTag, tempLine = "";
        Matcher matcherFindOutTags, mtCheckContent;
        List<String> tempResults = new ArrayList<String>();
        boolean bThereIsSubTag = false;
        boolean bThereIsMatchCheckContent = false;
        for (String tag : tagsOfLine) {
            hashedTag = String.valueOf(tag.hashCode());
            rgxCheckContent = "<" + hashedTag + ">" + rgxCheckTagContent + "</" + hashedTag + ">";
            if(bThereIsSubTag) {
                tempLine = tempLine.replace(tag, hashedTag);
            } else {
                tempLine = line.replace(tag, hashedTag);
            }

            patternCheckContent = Pattern.compile(rgxCheckContent);

            mtCheckContent = patternCheckContent.matcher(tempLine);
            bThereIsMatchCheckContent = mtCheckContent.find();
            if (bThereIsMatchCheckContent) {
                s = mtCheckContent.group(1);
                matcherFindOutTags = patternFindOutTags.matcher(s);
                bThereIsSubTag = matcherFindOutTags.find();

                if (!bThereIsSubTag && !s.isEmpty()) {
                    tempResults.add(s);
                }
                if(bThereIsSubTag){
                    tempLine=s;
                }
            } else {
                bThereIsSubTag = false;
            }
        }

        return tempResults;
    }

    public static void main(String[] args) throws IOException {
        String line;
        int i = 0;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/codemdg/tagextractor/input/input03.txt"));
            List<String> tempResults = new ArrayList<String>();
            while ((line = bufferedReader.readLine()) != null) {
                if (i > 0) {
                    tempResults = extractor(line, tagsExtractor(line));

                    if (tempResults.isEmpty()) {
                        results.add("None ");
                    } else {
                        results.addAll(tempResults);
                    }
                }
                i++;
            }
            results.forEach(System.out::println);

            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

