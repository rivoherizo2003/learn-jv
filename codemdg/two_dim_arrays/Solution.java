package codemdg.two_dim_arrays;

import java.io.*;
import java.util.*;
import java.util.stream.*;
import static java.util.stream.Collectors.toList;

class Result {
    public static int hourglassSum(List<List<Integer>> arr) {
        int row = 0;
        int col = 5;
        System.out.println("-----------HOUR GLASS-----------");
        arr.forEach(System.out::println);
        System.out.println("--------------------------------");
        List<Integer> hourGlassNumber = new ArrayList<>();
        for (List<Integer> l : arr) {
            for (int i = 0; i < col; i++) {
                List<Integer> subHourGlass = new ArrayList<>();
                if ((i+2) <= col && (row+2) <= col){
                    subHourGlass.add(l.get(i));
                    subHourGlass.add(l.get(i+1));
                    subHourGlass.add(l.get(i+2));

                    subHourGlass.add(arr.get(row+1).get(i+1));

                    subHourGlass.add(arr.get(row+2).get(i));
                    subHourGlass.add(arr.get(row+2).get(i+1));
                    subHourGlass.add(arr.get(row+2).get(i+2));

                    hourGlassNumber.add(subHourGlass.stream().reduce(0, Integer::sum));
                }
            }
            row++;
        }

        if (hourGlassNumber.stream().max(Comparator.comparing(Integer::valueOf)).isPresent()){
            return hourGlassNumber.stream().max(Comparator.comparing(Integer::valueOf)).get();
        } else {
            return 0;
        }
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/ozireh/ozireh/Projects/learn-jv/codemdg.two_dim_arrays/input00.txt"));

        List<List<Integer>> arr = new ArrayList<>();

        IntStream.range(0, 6).forEach(i -> {
            try {
                arr.add(
                    Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                        .map(Integer::parseInt)
                        .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        System.out.println(Result.hourglassSum(arr));

        bufferedReader.close();
    }
}

