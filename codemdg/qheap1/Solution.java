package codemdg.qheap1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.TreeSet;

public class Solution {
    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        String[] arr;
        Integer elm;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int testCases = Integer.parseInt(br.readLine());
        TreeSet<Integer> result = new TreeSet<Integer>();

        while(testCases > 0){
            arr = br.readLine().split(" ");
            elm = arr.length > 1 ? Integer.parseInt(arr[1]):0;

            switch(arr[0]){
                case "1":
                    result.add(elm);
                    break;
                case "2":
                    result.remove(elm);
                    break;
                case "3":
                    System.out.println(result.first());
                    break;
            }

            testCases--;
        }

    }
}
