package codemdg.flipmatrix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

class Result {

    /*
     * Complete the 'flippingMatrix' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY matrix as parameter.
     */

    public static int flippingMatrix(List<List<Integer>> matrix) {
        int sum = 0, max= 0;
        int n = matrix.size();
        System.out.println("size "+n);
        /**
         * Solution from
         * https://medium.com/@subhanusroy/flipping-the-matrix-hackerrank-optimised-solution-in-c-java-python-with-explanation-a3dad70149ee
         */
        for (int i = 0; i < n/2; i++) {
            for (int j = 0; j < n/2; j++) {
                max = Math.max(matrix.get(i).get(j),
                        Math.max(matrix.get(i).get(n - 1 - j),
                        Math.max(matrix.get(n - 1 - i).get(j),
                        matrix.get(n - 1 - i).get(n - 1 - j))));

                                System.out.println(i+" "+ max+ " "+matrix.get(i).get(j)+" "+matrix.get(i).get(n - 1 - j)+" "+matrix.get(n - 1 - i).get(j)+" "+matrix.get(n - 1 - i).get(n - 1 - j) );
                                sum+=max;
            }
        }
        return sum;
    }
}

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(System.getProperty("user.dir") + "/codemdg/flipmatrix/input/input00.txt"));

        int q = Integer.parseInt(bufferedReader.readLine().trim());

        for (int qItr = 0; qItr < q; qItr++) {
            int n = Integer.parseInt(bufferedReader.readLine().trim());

            List<List<Integer>> matrix = new ArrayList<>();

            for (int i = 0; i < 2 * n; i++) {
                String[] matrixRowTempItems = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

                List<Integer> matrixRowItems = new ArrayList<>();

                for (int j = 0; j < 2 * n; j++) {
                    int matrixItem = Integer.parseInt(matrixRowTempItems[j]);
                    matrixRowItems.add(matrixItem);
                }

                matrix.add(matrixRowItems);
            }

            int result = Result.flippingMatrix(matrix);
            System.out.println(result);
        }
    }
}
