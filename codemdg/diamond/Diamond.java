package codemdg.diamond;

import java.util.Scanner;

public class Diamond {
    protected static void diamondDraw(int N){
        String c = "#";
        String s = " ";
        int newN = (N*2)-1;
        int k =0;
        int space = (N-1);
        int nbrC = 1;
        int N2 = N-1;
        System.out.println(c.repeat(N));
        while (k <= newN){
            if (k == N2){
                space = 0;
                nbrC = newN;
            }
            if (space > 0 && nbrC > 0) {
                System.out.println(s.repeat(space)+c.repeat(nbrC));
            }

            if (k <= N2 && space > 0){
                space--;
                nbrC += 2;
            } else {
                space++;
                nbrC -=2;
            }
            k++;
        }
    }

    public static void main(String[] args) {
        Scanner scn = null;
        try{
            scn = new Scanner(System.in);
            System.out.print("Enter N:");
            int N = scn.nextInt();
            diamondDraw(N);
        } finally{
            if (scn != null){
                scn.close();
            }
        }
    }
}
